class Patient < ApplicationRecord
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :mrn, presence: true

  has_many :encounters, dependent: :delete_all

  def get_mrn
    mrn
  end
end
