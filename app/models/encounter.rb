class Encounter < ApplicationRecord
  belongs_to :patient
  
  validates :patient_id, presence: true
  validates :visit_number, presence: true
  validates :admitted_at, presence: true
  validates :discharged_at, presence: true

end
