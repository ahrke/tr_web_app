class DeleteEncounter < BaseInteractor
  def call
  	validate_required_params :encounter_id

    encounter_repo = EncounterRepository.new
    encounter_repo.destroy context.encounter_id
  end
end