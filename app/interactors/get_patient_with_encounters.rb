class GetPatientWithEncounters
  include Interactor::Organizer

  organize GetPatientById, GetPatientEncounters
end