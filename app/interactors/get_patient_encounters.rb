class GetPatientEncounters < BaseInteractor
  def call
  	validate_required_params :patient_id

    encounter_repo = EncounterRepository.new
    patients_encounters = encounter_repo.where_patient_id context.patient_id

    context.patients_encounters = patients_encounters
  end
end