class BaseInteractor
  include Interactor

  MISSING_PARAMETER = 'Missing required parameters:'.freeze

  def validate_required_params(*param_keys, **options)
    missing_parameters = missing_params param_keys
    if missing_parameters.any?
      context.fail! message: format_required_params_message(missing_parameters, options[:message])
    end
  end

  def missing_params(param_keys)
    param_keys.select { |required_key| context[required_key].nil? }
  end

  private

  def format_required_params_message(missing_params, message)
    message ||= MISSING_PARAMETER
    "#{message} #{missing_params.join(', ')}"
  end
end