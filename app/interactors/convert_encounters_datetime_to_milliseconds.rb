class ConvertEncountersDatetimeToMilliseconds < BaseInteractor
    def call 
      encounters = context.patients_encounters
      encounters = encounters.map{|encounter| 
          encounter.admitted_at = encounter.admitted_at.to_f * 1000
          encounter.discharged_at = encounter.discharged_at.to_f * 1000

          encounter
      }

      context.patients_encounters = encounters
    end
end