class UpdateEncounter < BaseInteractor
  def call
    validate_required_params :encounter_id, :new_values

    encounter_repo = EncounterRepository.new
    encounter = encounter_repo.update(context.encounter_id, context.new_values)

    context.encounter = encounter
  end
end