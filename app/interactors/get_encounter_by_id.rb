class GetEncounterById < BaseInteractor
  def call
    validate_required_params :encounter_id

    encounter_repository = EncounterRepository.new
    encounter = encounter_repository.find_by_id context.encounter_id

    unless encounter
      context.fail!(message: "Could not find encounter with id #{context.encounter_id}")
    end
    
    context.encounter = encounter
  end
end