class UpdatePatient < BaseInteractor
  def call
    validate_required_params :patient_id, :new_values

    patient_repo = PatientRepository.new
    patient = patient_repo.update(context.patient_id, context.new_values)

    context.patient = patient
  end
end