class GetAllPatients < BaseInteractor
  def call
    patient_repo = PatientRepository.new
    context.patients = patient_repo.all
  end
end