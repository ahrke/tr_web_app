class GetPatientById < BaseInteractor
  def call
    validate_required_params :patient_id

    patient_repository = PatientRepository.new
    patient = patient_repository.find_by_id context.patient_id

    unless patient
      context.fail!(message: "Could not find patient with id #{context.patient_id}")
    end
    
    context.patient = patient
  end
end