class DeletePatient < BaseInteractor
  def call
  	validate_required_params :patient_id

    patient_repo = PatientRepository.new
    patient_repo.destroy context.patient_id
  end
end