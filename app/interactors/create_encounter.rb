class CreateEncounter < BaseInteractor
  def call
  	validate_required_params :new_encounter

    encounter_repo = EncounterRepository.new 
    encounter = encounter_repo.create context.new_encounter

    context.encounter = encounter
  end
end