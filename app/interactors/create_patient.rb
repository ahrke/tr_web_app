class CreatePatient < BaseInteractor
  def call
  	validate_required_params :new_patient

    patient_repo = PatientRepository.new 
    patient = patient_repo.create context.new_patient

    context.patient = patient
  end
end