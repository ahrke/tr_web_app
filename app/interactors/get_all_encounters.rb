class GetAllEncounters < BaseInteractor
  def call
    encounter_repo = EncounterRepository.new

    encounters = encounter_repo.all

    context.encounters = encounters
  end
end