class PatientRepository
  def all
    Patient.all
  end

  def find_by_id id
    Patient.where(id: id).first
  end

  def destroy id
    Patient.destroy id
  end

  def update (id, new_values)
    Patient.update(id, new_values)
  end

  def create new_patient
    Patient.create! new_patient
  end
end
