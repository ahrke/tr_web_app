class EncounterRepository
  def all
    Encounter.all
  end

  def find_by_id encounter_id
    Encounter.where(id: encounter_id).first
  end

  def where_patient_id patient_id
  	return nil if !patient_id
    Encounter.where(patient_id: patient_id)
  end

  def create new_encounter
    Encounter.create! new_encounter
  end

  def update(encounter_id, new_values)
    Encounter.update(encounter_id, new_values)
  end

  def destroy encounter_id
    Encounter.destroy encounter_id
  end
end