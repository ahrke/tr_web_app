class EncountersController < ApplicationController
  before_action :settings
  protect_from_forgery with: :null_session

  def settings
    @encounters = EncounterRepository.new
  end

  def index
    results = GetAllEncounters.call
    if results.success? then
      render json: results.encounters
    else
      render json: { error: "Could not retrieve encounters" }
    end
  end

  def show
    result = GetEncounterById.call encounter_id: params[:id]
    if result.success? then
      render json: result.encounter
    else
      render json: { error: "Could not retrieve encounter" }
    end
  end

  def create
    result = CreateEncounter.call new_encounter: encounter_params

    if result.success? then
      render json: result.encounter
    else
      render json: { error: "Could not create encounter" }
    end
  end

  def update
    result = UpdateEncounter.call encounter_id: params[:id], new_values: encounter_params

    if result.success? then
      render json: result.encounter
    else
      render json: { error: "Could not update encounter #{params[:id]}" }
    end
  end

  def destroy
    result = DeleteEncounter.call encounter_id: params[:id]
    if result.success? then
      render json: result.encounter
    else
      render json: { error: "Could not delete encounter" }
    end
  end

  private

  def encounter_params
    params.require(:encounter).permit(
      :patient_id,
      :visit_number,
      :admitted_at,
      :discharged_at,
      :location,
      :room,
      :bed
    )
  end
end
