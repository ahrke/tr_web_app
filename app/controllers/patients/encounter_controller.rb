class Patients::EncounterController < ApplicationController
  def index
    encounters = GetPatientWithEncounters.call patient_id: params[:id]
    patient_with_encounters = {}
    patient_with_encounters[:patient] = encounters.patient.as_json
    patient_with_encounters[:encounters] = encounters.patients_encounters.as_json
    render json: patient_with_encounters
  end
end