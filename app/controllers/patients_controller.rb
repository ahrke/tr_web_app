class PatientsController < ApplicationController
  before_action :settings
  protect_from_forgery with: :null_session

  def settings
    @patient_repo = PatientRepository.new
  end

  def index
    results = GetAllPatients.call
    if results.success?
      render json: { patients: results.patients, status: results.server_status }, status: :ok 
    else
      render message: "error retrieving collection of patients" 
    end
  end

  def show
    result = GetPatientById.call patient_id: params[:id]
    if result.success?
      render json: { patient: result.patient, status: result.server_status }, status: :ok 
    else
      render message: "error retrieving patient" 
    end
  end

  def create
    result = CreatePatient.call new_patient: patient_params
    if result.success?
      render json: { patient: result.patient, status: result.server_status }, status: :ok 
    else
      render message: "error creating new patient" 
    end
  end

  def update
    result = UpdatePatient.call patient_id: params[:id], new_values: patient_params
    if result.success?
      render json: { patient: result.patient, status: result.server_status }, status: :ok 
    else
      render message: "error updating patient" 
    end
  end

  def destroy
    result = DeletePatient.call patient_id: params[:id]
    if result.success?
      render json: { patient: result.patient, status: result.server_status }, status: :ok 
    else
      render message: "error deleting patient" 
    end
  end

  private

  def patient_params
    params.require(:patient).permit(
    	:first_name,
    	:middle_name,
    	:last_name,
    	:weight,
    	:height,
    	:mrn
    )
  end
end
