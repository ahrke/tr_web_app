require 'test_helper'

class EncounterTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @encounter = build(:encounter)
  end

  test "#Encounter.create() successfully creates a new encounter with proper params" do
    nPatient = create :patient
  	nEncounter = create :encounter, patient_id: 1

  	assert nEncounter.valid?
  end

  test "#Encounter.create() fails to create with improper params" do
  	nEncounter = Encounter.create({
  	})

  	refute nEncounter.valid?
  end
end
