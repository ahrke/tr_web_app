require 'test_helper'

class PatientTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @patient = build :patient
  end

  test "#Patient.create!() succeeds to creates new patient with required parameters" do
  	nPatient = create :patient 
    
  	assert nPatient.valid?
  end

  test "#Patient.create!() fails if creates new patient with improper required params" do
  	nPatient = Patient.create({})

  	refute nPatient.valid?
  end

  test "#get_mrn returns mrn from patient" do
  	mrn = "4456609"
  	patient = create :patient, mrn: "#{mrn}"

  	assert_equal mrn, patient.get_mrn
  end
end
