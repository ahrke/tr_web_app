require 'test_helper'

class EncounterRepositoryTest < ActiveSupport::TestCase

  def setup
  	@encounter_repo = EncounterRepository.new

    @patient = create :patient
    @patient_id = @patient.id

    @encounter_id = 2
    @encounter = create :encounter, patient_id: @patient_id, id: @encounter_id, room: '5b'

  end

  test "#all returns all encounters" do
  	encounters_result = @encounter_repo.all

  	assert encounters_result
  end

  test "#find_by_id returns encounter with given id" do
    encounter_result = @encounter_repo.find_by_id @encounter_id

    assert_equal encounter_result, @encounter
  end

  test  "#find_by_id returns nil if no id exists" do
  	id = 10

  	encounter_result = @encounter_repo.find_by_id id

    assert_nil encounter_result
  end

  test "#get_patient_encounters returns all encounters" do
  	patient = create :patient, id: 10

  	5.times do
  	  create :encounter, patient_id: patient.id  
  	end

    patients_encounters = @encounter_repo.where_patient_id patient.id

    assert_equal patients_encounters.count, 5
  end

  test "#get_patient_encounters returns empty when invalid parameters are passed" do
  	patients_encounters = @encounter_repo.where_patient_id patient_id: nil

  	assert_equal patients_encounters.count, 0
  end


  test "#create succeeds to creates new encounter with required parameters" do
    mock_encounter = {
      patient_id: @patient_id,
      visit_number: 4,
      admitted_at: DateTime.new(2019, 12, 12, 12, 30, 0),
      discharged_at: DateTime.new(2019, 12, 12, 12, 40, 0)
    }

  	encounter = @encounter_repo.create mock_encounter
    
  	assert encounter.valid?
  end

  test "#update will update encounter room" do
 
    encounter_result = @encounter_repo.find_by_id @encounter_id

    assert_equal @encounter.room, encounter_result.room

    new_room = "2d"
    @encounter_repo.update(@encounter_id, { :room => new_room })
    encounter_result = @encounter_repo.find_by_id @encounter_id

    assert_equal encounter_result.room, new_room
  end

  test "#destroy will delete encounter" do

  	encounter_result = @encounter_repo.find_by_id @encounter_id

    # check that we've created encounter
  	assert encounter_result

    @encounter_repo.destroy @encounter_id
  	encounter_result = @encounter_repo.find_by_id @encounter_id

    # check that we've destroyed encounter
    refute encounter_result
  end

end