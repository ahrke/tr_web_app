require 'test_helper'

class PatientRepositoryTest < ActiveSupport::TestCase

  def setup
  	@patient_repo = PatientRepository.new

    @patient_id = 2
    @patient = create :patient, id: @patient_id
  end

  test "#all returns all patients" do
  	# mock_patients = create(:patient)

  	patients_result = @patient_repo.all

  	# assert_equal patients_result, mock_patients
  	assert patients_result
  end

  test "#returns patient with given id" do

    patient_result = @patient_repo.find_by_id @patient_id

    assert_equal patient_result, @patient
  end

  test  "#find_by_id returns nil if no id exists" do
  	id = 10

  	patient_result = @patient_repo.find_by_id id

    assert_nil patient_result
  end


  test "#succeeds to creates new patient with required parameters" do
    mock_patient = {
      first_name: "Sam",
      last_name: "Hero",
      mrn: "40PA23"
    }
  	nPatient = @patient_repo.create mock_patient
    
  	assert nPatient.valid?
  end

  test "#update will update patient name" do
 
    patient_result = @patient_repo.find_by_id @patient_id

    assert_equal @patient.first_name, patient_result.first_name

    new_first_name = "ninja"
    @patient_repo.update(@patient_id, { :first_name => new_first_name })
    patient_result = @patient_repo.find_by_id @patient_id

    assert_equal patient_result.first_name, new_first_name
  end

  test "#destroy will delete patient" do

  	patient_result = @patient_repo.find_by_id @patient_id

    # check that we've created patient
  	assert patient_result

    @patient_repo.destroy @patient_id
  	patient_result = @patient_repo.find_by_id @patient_id

    # check that we've destroyed patient
    refute patient_result
  end

end