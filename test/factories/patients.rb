FactoryBot.define do
  factory :patient do
  	id { 1 }
  	first_name { 'pickle' }
  	middle_name { 'R' }
  	last_name { 'rick' }
  	weight { 90 }
  	height { 40 }
  	mrn { 'testMRN009' }
  end
end