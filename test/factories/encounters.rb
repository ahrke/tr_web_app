FactoryBot.define do
  factory :encounter do
    patient_id { 980190963 }
    visit_number { '1' }
    admitted_at { DateTime.new(2019, 12, 12, 12, 30, 0) }
    discharged_at { DateTime.new(2019, 12, 12, 12, 50, 0) }
  end
end