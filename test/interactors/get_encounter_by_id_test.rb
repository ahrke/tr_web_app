class GetEncounterByIdTest < ActiveSupport::TestCase

  def setup
    @encounters_repo = EncounterRepository.new

    @patient = create :patient
    @patient_id = @patient.id

    @encounter_id = 2
    @encounter = create :encounter, id: @encounter_id, patient_id: @patient_id
  end

  test "#fails if no id is given" do
  	encounter_result = GetEncounterById.call {}

    refute encounter_result.success?
    assert encounter_result.message.include? GetEncounterById::MISSING_PARAMETER
  end

  test "#fails if can't find id" do
  	fake_encounter = { encounter_id: 10 }
  	encounter_result = GetEncounterById.call fake_encounter

  	refute encounter_result.success?
  	assert encounter_result.message.include? 'Could not find encounter with id'
  end

  test "#returns a encounter with proper id" do
  	mock_encounter = {
  		encounter_id: @encounter.id
  	}
  	encounter_result = GetEncounterById.call mock_encounter

  	assert encounter_result.success?
  	assert_equal encounter_result.encounter.id, @encounter.id
  end
end