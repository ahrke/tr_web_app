class DeleteEncounterTest < ActiveSupport::TestCase

  def setup
    @patient = create :patient
    @patient_id = @patient.id
  end

  test "#delete encounter with given id" do
  	encounter_repo = EncounterRepository.new
  	encounter = create :encounter, patient_id: @patient_id
  	eId = {
      encounter_id: encounter.id
  	}
    
    encounter_result = encounter_repo.find_by_id eId[:encounter_id]

    assert encounter_result.valid?

    DeleteEncounter.call eId
    encounter_result = encounter_repo.find_by_id eId[:encounter_id]

    refute encounter_result
  end
end