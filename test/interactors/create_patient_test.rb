class CreatePatientTest < ActiveSupport::TestCase
  test "#creates a new user with given valid parameters" do
  	mock_patient = {
  	  new_patient: {
        first_name: 'Sam',
        last_name: 'hero',
        mrn: '0099303'
  	  }
  	}
  	nPatient = CreatePatient.call mock_patient

  	assert nPatient.patient.valid?
  end

  test "#fails to create a user with invalid parameters" do
  	mock_patient_no_params = {
  	}
  	nPatient = CreatePatient.call mock_patient_no_params

    refute nPatient.success?
  	assert nPatient.message.include? CreatePatient::MISSING_PARAMETER 
  end
end