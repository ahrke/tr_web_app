class GetPatientEncountersTest < ActiveSupport::TestCase
  def setup
    @encounter_repo = EncounterRepository.new

  end

  test "#call should return all of patient's encounters" do
    patient = create :patient, id: 10
    5.times do 
      create :encounter, patient_id: patient.id
    end
  	result = GetPatientEncounters.call patient_id: patient.id

  	assert_equal result.patients_encounters.count, 5
  end
end