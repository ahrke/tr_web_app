class CreateEncounterTest < ActiveSupport::TestCase
  def setup
    @patient = create :patient
    @patient_id = @patient.id
  end

  test "#creates a new encounter with given valid parameters" do
  	mock_encounter = {
  	  new_encounter: {
        patient_id: @patient_id,
        visit_number: 4,
        admitted_at: DateTime.new(2019, 12, 12, 12, 30, 0),
        discharged_at: DateTime.new(2019, 12, 12, 12, 40, 0)
      }
  	}
  	encounter = CreateEncounter.call mock_encounter

  	assert encounter.encounter.valid?
  end

  test "#fails to create a user with invalid parameters" do
  	mock_encounter_no_params = {
  	}
  	encounter = CreateEncounter.call mock_encounter_no_params

    refute encounter.success?
  	assert encounter.message.include? CreateEncounter::MISSING_PARAMETER 
  end
end