class DeletePatientTest < ActiveSupport::TestCase
  test "#deletes patient with given id" do
  	patient_repo = PatientRepository.new
  	patient = create :patient
  	pId = {
      patient_id: patient.id
  	}
    
    patient_result = patient_repo.find_by_id pId[:patient_id]

    assert patient_result.valid?

    DeletePatient.call pId
    patient_result = patient_repo.find_by_id pId[:patient_id]

    refute patient_result
  end
end