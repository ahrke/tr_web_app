class GetPatientByIdTest < ActiveSupport::TestCase

  def setup
    @patients_repo = PatientRepository.new

    @patient_id = 2
    @patient = create :patient, id: @patient_id
  end

  test "#fails if no id is given" do
  	patient_result = GetPatientById.call {}

    refute patient_result.success?
    assert patient_result.message.include? GetPatientById::MISSING_PARAMETER
  end

  test "#fails if can't find id" do
  	fake_patient = { patient_id: 10 }
  	patient_result = GetPatientById.call fake_patient

  	refute patient_result.success?
  	assert patient_result.message.include? 'Could not find patient with id'
  end

  test "#returns a patient with proper id" do
  	mock_patient = {
  		patient_id: @patient.id
  	}
  	patient_result = GetPatientById.call mock_patient

  	assert patient_result.success?
  	assert_equal patient_result.patient.id, @patient.id
  end
end