class GetAllPatientsTest < ActiveSupport::TestCase
  test "#all returns all patients" do
  	patient_repo = PatientRepository.new

  	curr_count = patient_repo.all.count
    
    5.times do |patient|
      create :patient, id: patient
    end

  	patients_result = GetAllPatients.call
    
    assert_equal patients_result.patients.count, 5 + curr_count
  end
end