class UpdateEncounterTest < ActiveSupport::TestCase

  def setup
    @patient = create :patient
    @patient_id = @patient.id
  end

  test "#updates given user with new name" do
  	encounter_repo = EncounterRepository.new
  	room = '5c'
  	encounter = create :encounter, room: room, patient_id: @patient_id
  	encounter_id = encounter.id

  	encounter_result = encounter_repo.find_by_id encounter_id

  	assert_equal encounter_result.room, room

    new_room = '1d'
  	UpdateEncounter.call(encounter_id: encounter_id, new_values: { room: new_room })
  	encounter_result = encounter_repo.find_by_id encounter_id

  	assert_equal encounter_result.room, new_room
  end
end