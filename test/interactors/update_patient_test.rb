class UpdatePatientTest < ActiveSupport::TestCase
  test "#updates given user with new name" do
  	patient_repo = PatientRepository.new
  	first_name = 'Sur'
  	patient = create :patient, first_name: first_name
  	patient_id = patient.id

  	patient_result = patient_repo.find_by_id patient_id

  	assert_equal patient_result.first_name, first_name

    new_first_name = 'Orion'
  	UpdatePatient.call(patient_id: patient_id, new_values: { first_name: new_first_name })
  	patient_result = patient_repo.find_by_id patient_id

  	assert_equal patient_result.first_name, new_first_name
  end
end