Rails.application.routes.draw do
  root 'patients#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :patients, :encounters
  namespace :patients do
    get '/:id/encounters', to: 'encounter#index'
  end

end
